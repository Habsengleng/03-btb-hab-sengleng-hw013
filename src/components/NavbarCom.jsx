import React, { Component } from "react";
import { Navbar, Nav, Form, FormControl } from "react-bootstrap";
import { Link } from "react-router-dom";

class NavbarCom extends Component {
  constructor() {
    super();
    this.state = {
      search:'',
    };
  }
  handleChange = (e) => {
    this.setState({search:e.target.value},()=>{
      console.log(this.state.search);
      this.props.onSearch(this.state.search);
    })
  };
  render() {
    return (
      <div>
        <Navbar bg="light" expand="lg">
          <div className="container">
            <Navbar.Brand as={Link} to="/">
              AMS
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
              <Nav className="mr-auto">
                <Nav.Link as={Link} to="">
                  Home
                </Nav.Link>
              </Nav>
              <Form inline>
                <FormControl
                  type="text"
                  placeholder="Search"
                  className="mr-sm-2"
                  name="search"
                  value={this.state.search}
                  onChange={(e) => this.handleChange(e)}
                />
              </Form>
            </Navbar.Collapse>
          </div>
        </Navbar>
      </div>
    );
  }
}

export default NavbarCom;
