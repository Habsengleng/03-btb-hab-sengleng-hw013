import React from "react";
import TableData from "./TableData";
import { Button,Spinner } from "react-bootstrap";
import { Link } from "react-router-dom";

export default function AdminPage(props) {
  if (props.loading) {
    return (
      <center>
        <Button variant="info" disabled>
          <Spinner
            as="span"
            animation="border"
            size="md"
            role="status"
            aria-hidden="true"
          />
          <span className="sr-only">Loading...</span>
        </Button>{" "}
        <Button variant="info" disabled>
          <Spinner
            as="span"
            animation="grow"
            size="md"
            role="status"
            aria-hidden="true"
          />
          Loading...
        </Button>
      </center>
    );
  }
  return (
    <div className="container">
      <center>
        <h1>Article Management</h1>
      </center>
      <center>
        <Button variant="dark" as={Link} to="/add">
          Add New Article
        </Button>
      </center>
      <br />
      <TableData data={props.dataSearch} onDelete={props.onDelete} />
    </div>
  );
}
