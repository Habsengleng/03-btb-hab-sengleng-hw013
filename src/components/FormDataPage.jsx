import React, { Component } from "react";
import { Form, Button } from "react-bootstrap";
import Axios from "axios";
import Image from "./img/Default_Image_Thumbnail.png";

export default class FormDataPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      TITLE: "",
      DESCRIPTION: "",
      IMAGE: Image,
    };
  }

  onImageChange = (event) => {
    console.log(event);
    if (event.target.files && event.target.files[0]) {
      this.setState({
        IMAGE: URL.createObjectURL(event.target.files[0]),
      });
    }
  };
  changeHandler = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };
  onAdd = () => {
    console.log(this.state);
    let title = this.state.TITLE;
    let desc = this.state.DESCRIPTION;
    if (this.state.IMAGE === Image) {
      alert("Please choose Picture");
      return false;
    } else {
      if (title !== "") {
        if (desc !== "") {
          Axios.post("http://110.74.194.124:15011/v1/api/articles", this.state)
            .then((res) => {
              alert(res.data.MESSAGE);
              console.log(res.data.DATA.IMAGE);
              this.props.history.push("/");
            })
            .catch((err) => {
              alert(err);
            });
        } else {
          document.getElementById("errorTitle").innerHTML = "";
          this.descInput.focus();
          document.getElementById("errorDesc").innerHTML =
            "Please input description";
        }
      } else {
        document.getElementById("errorDesc").innerHTML = "";
        this.titleInput.focus();
        document.getElementById("errorTitle").innerHTML = "Please input title";
      }
    }
  };
  render() {
    return (
      <div className="container ">
        <h1>Add Article</h1>
        <div className="row">
          <div className="col-lg-8">
            <Form>
              <Form.Group controlId="formBasicEmail">
                <Form.Label>Title</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter Title"
                  name="TITLE"
                  value={this.state.TITLE}
                  onChange={this.changeHandler}
                  ref={(input) => {
                    this.titleInput = input;
                  }}
                />
              </Form.Group>
              <small style={{ color: "red" }} id="errorTitle"></small>

              <Form.Group controlId="formBasicPassword">
                <Form.Label>Description</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter Description"
                  name="DESCRIPTION"
                  value={this.state.DESCRIPTION}
                  onChange={this.changeHandler}
                  ref={(input) => {
                    this.descInput = input;
                  }}
                />
              </Form.Group>
              <small style={{ color: "red" }} id="errorDesc"></small>
            </Form>
          </div>
          <div className="col-lg-4">
            <img
              id="target"
              alt=""
              src={this.state.IMAGE}
              className="w-100 p-3"
            />
            <input
              type="file"
              onChange={this.onImageChange}
              className="filetype"
              id="group_image"
            />
          </div>
        </div>
        <div className="row">
          <div className="col-lg-12">
            <Button
              variant="primary"
              type="button"
              onClick={() => this.onAdd()}
            >
              Add
            </Button>
          </div>
        </div>
      </div>
    );
  }
}
