import React from "react";
import Image from "./img/Default_Image_Thumbnail.png";

export default function ViewPage({ match, data }) {
  console.log(data);
  console.log(match.params.id);
  var dataView = data.find((d) => d.ID == match.params.id);
  console.log(dataView.IMAGE);
  return (
    <div className="container">
      <h1>Article</h1>
      <div className="row">
        <div className="col-lg-4">
          {dataView.IMAGE == null ? (
            <img src={Image} alt="" className="w-100 p-3" />
          ) : (
            <img src={dataView.IMAGE} alt="" className="w-100 p-3" />
          )}
        </div>
        <div className="col-lg-8">
          <h3>{dataView.TITLE}</h3>
          <p>{dataView.DESCRIPTION}</p>
        </div>
      </div>
    </div>
  );
}
