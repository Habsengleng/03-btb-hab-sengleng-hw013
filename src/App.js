import React, { Component } from "react";
import Axios from "axios";
import AdminPage from "./components/AdminPage";
import NavbarCom from "./components/NavbarCom";
import { Route, Switch, BrowserRouter as Router } from "react-router-dom";
import FormDataPage from "./components/FormDataPage";
import ViewPage from "./components/ViewPage";
import UpdatePage from "./components/UpdatePage";

export default class App extends Component {
  constructor() {
    super();
    this.state = {
      loading: true,
      data: [],
      isUpdate: false,
      title:''
    };
  }
  componentDidMount() {
    Axios.get("http://110.74.194.124:15011/v1/api/articles?page=1&limit=30")
      .then((res) =>
        this.setState({
          loading: false,
          data: res.data.DATA,
        })
      )
      .catch((err) => {
        alert(err);
      });
    console.log(this.state.data);
  }
  componentWillUpdate() {
    Axios.get("http://110.74.194.124:15011/v1/api/articles?page=1&limit=30")
      .then((res) =>
        this.setState({
          loading: false,
          data: res.data.DATA,
        })
      )
      .catch((err) => {
        alert(err);
      });
    console.log(this.state.data);
  }
  onDelete = (deleteID) => {
    let datas = this.state.data.filter((data) => data.ID !== deleteID);
    this.setState({
      data: datas,
    });
  };
  searchByTitle = (title) => {
    this.setState({
     title:title
    })
  };
  render() {
    let filterItem = this.state.data.filter((item) => {
      return item.TITLE.toLowerCase().indexOf(this.state.title.toLowerCase()) !== -1;
    });
    return (
      <div>
        <Router>
          <NavbarCom data={this.state.data} onSearch={this.searchByTitle} />
          <Switch>
            <Route
              path="/add"
              render={(props) => (
                <FormDataPage
                  {...props}
                  onUpdate={this.onUpdate}
                />
              )}
            />
            <Route
              path={"/view/:id"}
              render={(props) => <ViewPage {...props} data={this.state.data} />}
            />
            <Route
              path={"/update/:id"}
              render={(props) => (
                <UpdatePage
                  {...props}
                  data={this.state.data}
                />
              )}
            />
            <AdminPage
              dataSearch={filterItem}
              loading={this.state.loading}
              onDelete={this.onDelete}
            />
          </Switch>
        </Router>
      </div>
    );
  }
}
